module golang-gitlab-pipeline

go 1.13

require (
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/mux v1.6.2
)
